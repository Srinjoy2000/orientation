# Dockers Summary

**Docker** is a container management service. Docker is an open platform for developing, shipping, and running applications.

- A Docker image that can be deployed to any docker enviornment is contains everything needed to run an applications as a container. This includes:

code
runtime
libraries
environment variables
configuration files

- A Docker container is a running Docker image.From one image you can create multiple containers .

Docker Commands :


- docker ps -  to view all the containers that are running on the Docker Host

- docker start -  starts any stopped container(s).

- docker stop - stops any running container(s).

- docker run - creates containers from docker images.

- docker rm - deletes the containers.

## Docker Workflow
![Step-by-step workflow for developing Docker containerized apps](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/docker-application-development-process/media/docker-app-development-workflow/life-cycle-containerized-apps-docker-cli.png)
![](https://i1.wp.com/www.docker.com/blog/wp-content/uploads/2019/06/948c0bc7-d107-486e-a2e6-59b131ee4e40-1.jpg?ssl=1)
